package com.android.example.notekeeper;

import android.content.Context;
import android.content.Intent;

/**
 * Created by sagar on 12/1/18.
 */

public class CourseEventBroadcastHelper {

    public static final String ACTION_COURSE_EVENT = "com.android.example.notekeeper.action.COURSE_EVENT";

    public static final String COURSE_ID = "com.android.example.notekeeper.extra.COURSE_ID";
    public static final String EXTRA_COURSE_MESSAGE = "com.android.example.notekeeper.extra.COURSE_MESSAGE";

    public static void sendEventBroadcast(Context context, String courseId, String message) {
        Intent intent = new Intent(ACTION_COURSE_EVENT);
        intent.putExtra(COURSE_ID, courseId);
        intent.putExtra(EXTRA_COURSE_MESSAGE, message);

        context.sendBroadcast(intent);
    }

}
